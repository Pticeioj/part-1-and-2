﻿Функция ПолучитьМакетОбработки(Имя) Экспорт
	Возврат ПолучитьМакет(Имя);
КонецФункции
Функция СведенияОВнешнейОбработке() Экспорт
	
	ПараметрыРегистрации = ДополнительныеОтчетыИОбработки.СведенияОВнешнейОбработке();
	
	ПараметрыРегистрации.Вид = ДополнительныеОтчетыИОбработкиКлиентСервер.ВидОбработкиЗаполнениеОбъекта();
	
	Возврат ПараметрыРегистрации;
	
КонецФункции